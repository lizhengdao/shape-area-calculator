﻿namespace ShapeAreaCalculator
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.ShapeOptionCombo = new System.Windows.Forms.ComboBox();
            this.SelectedShape_CalculateArea = new System.Windows.Forms.Button();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.SelectedShapeDescription = new System.Windows.Forms.Label();
            this.SelectedShapeAreaResult = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SelectedShapeFields_Grid = new System.Windows.Forms.DataGridView();
            this.Field = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedShapeFields_Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeOptionCombo
            // 
            this.ShapeOptionCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ShapeOptionCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ShapeOptionCombo.FormattingEnabled = true;
            this.ShapeOptionCombo.Location = new System.Drawing.Point(86, 3);
            this.ShapeOptionCombo.Name = "ShapeOptionCombo";
            this.ShapeOptionCombo.Size = new System.Drawing.Size(145, 21);
            this.ShapeOptionCombo.TabIndex = 0;
            this.ShapeOptionCombo.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.ShapeOptionCombo_Format);
            this.ShapeOptionCombo.SelectedValueChanged += new System.EventHandler(this.ShapeOptionCombo_SelectedValueChanged);
            // 
            // SelectedShape_CalculateArea
            // 
            this.SelectedShape_CalculateArea.Location = new System.Drawing.Point(512, 378);
            this.SelectedShape_CalculateArea.Name = "SelectedShape_CalculateArea";
            this.SelectedShape_CalculateArea.Size = new System.Drawing.Size(276, 48);
            this.SelectedShape_CalculateArea.TabIndex = 6;
            this.SelectedShape_CalculateArea.Text = "Calculate";
            this.SelectedShape_CalculateArea.UseVisualStyleBackColor = true;
            this.SelectedShape_CalculateArea.Click += new System.EventHandler(this.SelectedShape_CalculateArea_Click);
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.74468F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.25532F));
            this.TableLayoutPanel1.Controls.Add(this.ShapeOptionCombo, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.Label1, 0, 0);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(8, 65);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 1;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(235, 27);
            this.TableLayoutPanel1.TabIndex = 7;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(3, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(57, 21);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Shape";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.Label3.Location = new System.Drawing.Point(5, 27);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(432, 13);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "Select a shape and enter the required dimensions. Click Calculate to calculate ar" +
    "ea.";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(2, 2);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(207, 25);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "Shape Area Calculator";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.489758F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.51024F));
            this.tableLayoutPanel2.Controls.Add(this.SelectedShapeDescription, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 98);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(537, 49);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // SelectedShapeDescription
            // 
            this.SelectedShapeDescription.AutoSize = true;
            this.SelectedShapeDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.SelectedShapeDescription.Location = new System.Drawing.Point(3, 0);
            this.SelectedShapeDescription.Name = "SelectedShapeDescription";
            this.SelectedShapeDescription.Size = new System.Drawing.Size(117, 13);
            this.SelectedShapeDescription.TabIndex = 2;
            this.SelectedShapeDescription.Text = "<Shape Description>";
            // 
            // SelectedShapeAreaResult
            // 
            this.SelectedShapeAreaResult.AutoSize = true;
            this.SelectedShapeAreaResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectedShapeAreaResult.Location = new System.Drawing.Point(621, 178);
            this.SelectedShapeAreaResult.Name = "SelectedShapeAreaResult";
            this.SelectedShapeAreaResult.Size = new System.Drawing.Size(132, 21);
            this.SelectedShapeAreaResult.TabIndex = 13;
            this.SelectedShapeAreaResult.Text = "<Current Result>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(558, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 21);
            this.label7.TabIndex = 14;
            this.label7.Text = "Result";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 21);
            this.label5.TabIndex = 15;
            this.label5.Text = "Required Fields";
            // 
            // SelectedShapeFields_Grid
            // 
            this.SelectedShapeFields_Grid.AllowUserToAddRows = false;
            this.SelectedShapeFields_Grid.AllowUserToDeleteRows = false;
            this.SelectedShapeFields_Grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SelectedShapeFields_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SelectedShapeFields_Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Field,
            this.Value});
            this.SelectedShapeFields_Grid.Location = new System.Drawing.Point(8, 204);
            this.SelectedShapeFields_Grid.Name = "SelectedShapeFields_Grid";
            this.SelectedShapeFields_Grid.Size = new System.Drawing.Size(429, 222);
            this.SelectedShapeFields_Grid.TabIndex = 16;

            // 
            // Field
            // 
            this.Field.DataPropertyName = "Field";
            this.Field.HeaderText = "Field";
            this.Field.Name = "Field";
            this.Field.ReadOnly = true;
            // 
            // Value
            // 
            this.Value.DataPropertyName = "Value";
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.SelectedShapeFields_Grid);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.SelectedShapeAreaResult);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.SelectedShape_CalculateArea);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Shape Area Calculator";
            this.Load += new System.EventHandler(this.Main_Load);
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedShapeFields_Grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ComboBox ShapeOptionCombo;
        internal System.Windows.Forms.Button SelectedShape_CalculateArea;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label SelectedShapeDescription;
        private System.Windows.Forms.Label SelectedShapeAreaResult;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView SelectedShapeFields_Grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}

