﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeAreaCalculator.Model
{
    public class ShapeDataBindContainer
    {
        public string Field { get; set; }
        public double Value { get; set; }
    }
}
